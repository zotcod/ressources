# ZotCours

Bonjour et bienvenue sur le git de zot'cod pour référencer les projets de code.

Mettez ici, tous les liens utiles à cet effet.
A lire: https://classic.csunplugged.org/books/
- ,https://interstices.info/wp-content/uploads/2018/01/csunplugged2014-fr-comp.pdf
- https://interstices.info/wp-content/uploads/2018/01/csunplugged_part2_fr.pdf

## Idées de cours
Pour chaque idée de cours l'idée est d'avoir un lien vers le texte du cours et un lien vers le code sur ( [codepen](https://codepen.io) ou sur [l'éditeur p5js](https://editor.p5js.org)).

- bulles à cliquer: [[cours]()]  [[code]()]
- bulles drag and drop:  [[cours]()]  [[code]()]
- snake:  [[cours]()]  [[code](https://editor.p5js.org/t.poulain@rt-iut.re/sketches/BkyuRkcnX)]
- snake (voix):  [[cours]()]  [[code]()]
- Filtre instagram sur caméra:  [[cours]()]  [[code](https://editor.p5js.org/t.poulain@rt-iut.re/sketches/r1NvgEZCm)]
- Dessiner le drapeau de l'Union Jack:  [[cours]()]  [[code]()]
- Dessiner un cercle trigonométrique:  [[cours]()]  [[code]()]
- Calculer la surface d'un cercle par simulation:  [[cours]()]  [[code](https://editor.p5js.org/trazafin/sketches/BkFOhWchm)]
- Calculer la valeur de `pi` par simulation:  [[cours]()]  [[code](https://editor.p5js.org/t.poulain@rt-iut.re/sketches/S1pOQl52m)]
- une bulle qui suit la souris [[cours](https://www.overleaf.com/read/myzzpckdkphw)]  [[code](https://editor.p5js.org/t.poulain@rt-iut.re/sketches/By1w1-yTQ)]
- une horloge [[cours](https://www.overleaf.com/read/zxgxnvkxvkdf)]  [[code](https://editor.p5js.org/t.poulain@rt-iut.re/sketches/S1rEnTCaX)]
- un algorithme de tri de valeurs (bubble sort) [[cours]()]  [[code](https://editor.p5js.org/t.poulain@rt-iut.re/sketches/rJYEa7eA7)]
- Un sismographe:  [[cours]()]  [[code](https://editor.p5js.org/t.poulain@rt-iut.re/sketches/By9pM1QR7)]
- Le cristal des Sims sur la tête : [[code](https://editor.p5js.org/t.poulain@rt-iut.re/sketches/ryj7_7RyV)]
- Illustration de la suite de Fibonacci : [[code](https://editor.p5js.org/t.poulain@rt-iut.re/sketches/Xfu9RQaoU)]


## Liens

### Liens vers les tutoriaux intéressants:
- https://shiffman.net/a2z/intro/ (programmation de a à z)

#### A utiliser directement:
- Voir [la vidéo](https://www.youtube.com/watch?v=fBqaA7zRO58) avec les bulles et comment utiliser un tableau dans p5js, [le depot github](https://github.com/CodingTrain/website/tree/master/Tutorials/P5JS/p5.js/07) and [la video qui suit](https://www.youtube.com/watch?v=tA_ZgruFF9k) et le calcul de la [distance entre objets](https://www.youtube.com/watch?v=W1-ej3Wu5zg) pour l'interaction avec la souris est [ici](https://www.youtube.com/watch?v=5Q9cA0REztY)

- Un premier tuto sur les [Arrays et les objets](https://www.youtube.com/watch?v=fBqaA7zRO58)
	- function `mousePressed()`
	- function `mouseDragged()`
- [Un tuto sur l'interaction d'un array de bulle avec la souris](https://www.youtube.com/watch?v=TaN5At5RWH8)
	- Cliquer à l'intérieur d'une bulle et changer sa couleur
	- Utilisation de `mouseX`, `mouseY`  pour la position de la souris
- [Un tuto simple sur l'utilisation des arrays](https://www.youtube.com/watch?v=tA_ZgruFF9k)
	- array: push(), pop(), shift, unshift() : ce sont des fonction javascript qui faure recherche `mozilla fondation array javascript`
	- autre utilisation de `mouseDragged()`
- Capture vidéo. [ici](https://www.youtube.com/watch?v=bkGf4fEHKak) Il faut voir les vidéos suivantes du tuto.
	- Un tuto sur la capture vidéo capture vidéo
	- Comment insérer dans un canevas
	- Mettre un filtre de couleur.
	- [Nez rouge](https://editor.p5js.org/brunoruchiga/sketches/ByrqL6UJE) avec la vidéo [ici](https://www.youtube.com/watch?v=EA3-k9mnLHs)

- Son et Voix
	- text to speech, la vidéo est [ici](https://www.youtube.com/watch?v=v0CHV33wDsI)
	- speech to text,  la vidéo est [ici](https://www.youtube.com/watch?v=q_bXBcmfTJM).  Excellent pour faire un snake guidé par la voix.

- Vidéo [ici](https://www.youtube.com/watch?v=uNQSVU0IKec) sur les interactions basiques avec du html 
	- interaction avec les pages html
	- DOM
	- input, textarea, button, paragraph, etc...
	- Utilisation des `id` dans le code html.
- [Snake game](https://www.youtube.com/watch?v=OMoVcohRgZA)
- [Matrix rain](https://www.youtube.com/watch?v=S1TQCi9axzg)
- Circle packing
	- [Remplir du texte avec des bulles](https://www.youtube.com/watch?v=QHEQuoIKgNE)
	- Long (3h30) [tutorial a voir](https://www.youtube.com/watch?v=QHEQuoIKgNE)
	- [Avec coloration suivant une image de fond](https://www.youtube.com/watch?v=ERQcYaaZ6F0)
- creategraphics:; https://www.youtube.com/watch?v=pNDc8KXWp9E on dirait la creation de layer
- Bouncing ball : https://www.youtube.com/watch?v=vqE8DMfOajk, trail for an object

- Utiliser une carte pour la visualisation: - https://www.youtube.com/watch?v=Ae73YY_GAU8
- Effet goutte d'eau en 2d (fait avec processing): https://www.youtube.com/watch?v=BZUdGqeOD0w code en javascript ici: https://codepen.io/ds604/pen/VxroVN et ici: https://gist.github.com/ds604/228483d2498cdfdf79ef9df22676b899
- rita.js : https://www.youtube.com/watch?v=lIPEvh8HbGQ Analyse de phrase ne anglais. Peut être rigolo. Dommage que ce soit en anglais. En partant d'ici peut-être: https://bitbucket.org/romainvaleri/fr_sentence_gen
- Lissajou: https://www.youtube.com/watch?v=--6eyLO78CY p5js:  https://www.youtube.com/watch?v=--6eyLO78CY
- [Flocking](https://www.youtube.com/watch?v=mhjuuHl6qHM)

#### outils
- L'éditeur en ligne de p5js est [editor.p5js.org](https://editor.p5js.org)
- Utilisation de [codepen.io](https://codepen.io/) avec le tuto [ici](https://www.youtube.com/watch?v=5gfUgNpS6kY&t=210s)
- https://www.youtube.com/watch?v=HZ4D3wDRaec
	- Un tuto avec un exemple de workflow
- https://www.youtube.com/watch?v=lAtoaRz78I4 (creer de l'html avec p5)

#### pas utile pour le moment.
- https://www.youtube.com/watch?v=UiYb4yCsqFE mapping earthquake
- https://www.youtube.com/watch?v=pF0cadg2mg0 circle packing
- https://www.youtube.com/watch?v=yPWkPOfnGsw This one is recent. This is about learning to code.
- https://www.youtube.com/watch?v=JkznVy_Ciwk broadcasting system OpenBroadcastingSystem
- Polar to cartesian: https://thecodingtrain.com/Tutorials/9-additional-topics/9.20-polar-coordinates.html
- Circle morphing: https://www.youtube.com/watch?v=u2D4sxh3MTs

####  Processing au lycée - Python
- http://www.ac-grenoble.fr/disciplines/informatiquelycee/proc_index.html
- http://www.ac-grenoble.fr/disciplines/informatiquelycee/python_proc_a1.html


<!--stackedit_data:
eyJoaXN0b3J5IjpbMTQxMDYzNjE2MSwtMzc4NDM1NDEzLDg5Nj
EyMDk3MiwtNzQ0MjA2ODAwLDExMzk5MDU0OTIsNTk0MTQ0MTY1
LC0yOTk4Nzk0MjgsLTE0NzQ2MDcwODQsLTk2MDA4NjE0OCw2Nz
c4MjgyMTUsNjQ3OTM3NDg3LDEwOTA1ODYzOTUsLTU5OTEzMTYx
MiwtMTM4MTgwMjQxNywtMTAwMDQ5NDY0NCwtNTAxNDcyNDU2LD
IxMDE1MzIyNDVdfQ==
-->